package com.company;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Students firstStudent = new Students();
        firstStudent.setName("Oleg");
        StudyGroup group1 = new StudyGroup();
        group1.setGroupName("Recruiting & HR (MANAGEMENT)");
        List<StudyGroup> firstStudentGroup = new ArrayList<>();
        firstStudentGroup.add(group1);

        Students secondStudent = new Students();
        secondStudent.setName("Ivan");
        StudyGroup group3 = new StudyGroup();
        group3.setGroupName("Java Elementary (PROGRAMMING), QA (TESTING)");
        List<StudyGroup> secondStudentGroup = new ArrayList<>();
        secondStudentGroup.add(group3);


        Teacher firstTeacher = new Teacher();
        firstTeacher.setName("Stepan");
        StudyGroup group2 = new StudyGroup();
        group2.setGroupName("Java Elementary (PROGRAMMING)");
        List<StudyGroup> firstTeacherGroup = new ArrayList<>();
        firstTeacherGroup.add(group2);


        System.out.println(firstStudent +" "+group1);
        System.out.println(secondStudent +" "+group3);
        System.out.println(firstTeacher +" "+group2);



    }
}
